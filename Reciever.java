import com.rabbitmq.client.*;
import java.io.IOException;

public class Reciever {

    private final static String QUEUE_NAME = "cola";

    public static void main(String[] argv) throws Exception {
    	ConnectionFactory factory = new ConnectionFactory();
       factory.setHost("10.0.2.15");
       factory.setUsername("admin");
       factory.setPassword("admin");
       // factory.setVirtualHost("/");
       //factory.setHost("localhost");
       // factory.setPort(15672);
        
        
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println("Canal esperando mensjes, para salir presiona CTRL+C...");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" Mensaje Recibido!!! ->  '" + message + "'");
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
}