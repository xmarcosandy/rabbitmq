import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Scanner; 

import java.io.IOException;

import java.nio.charset.StandardCharsets;

public class Sender {

    private final static String QUEUE_NAME = "cola";

    public static void main(String[] argv) throws Exception {
    	
    	ConnectionFactory factory = new ConnectionFactory();
    	 factory.setHost("10.0.2.15");
         factory.setUsername("admin");
         factory.setPassword("admin");
       // factory.setVirtualHost("/");
        //factory.setHost("localhost");
       // factory.setPort(15672);

        try (Connection connection = factory.newConnection();
                Channel channel = connection.createChannel()) {
               channel.queueDeclare(QUEUE_NAME, false, false, false, null);
               
               System.out.println ("Iniciando envio de mensajes a cola");
               System.out.println ("Por favor introduzca el mensaje a enviar:");
               Scanner entradaEscaner = new Scanner (System.in);
               String message = entradaEscaner.nextLine ();
               
               channel.basicPublish("", QUEUE_NAME, null, message.getBytes(StandardCharsets.UTF_8));
               System.out.println(" Enviado! '" + message + "'");
           }
        
       
    }
}